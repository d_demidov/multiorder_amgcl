#include <iostream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/foreach.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/typeof/typeof.hpp>

#include <amgcl/backend/builtin.hpp>
#include <amgcl/runtime.hpp>
#include <amgcl/preconditioner/runtime.hpp>
#include <amgcl/make_solver.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/adapter/reorder.hpp>
#include <amgcl/io/mm.hpp>
#include <amgcl/io/binary.hpp>

#include <amgcl/profiler.hpp>

namespace amgcl { profiler<> prof; }
using amgcl::prof;
using amgcl::precondition;

typedef amgcl::scoped_tic< amgcl::profiler<> > scoped_tic;
typedef amgcl::backend::builtin<double> Backend;

//---------------------------------------------------------------------------
template <class MatrixA, class MatrixM>
boost::tuple<size_t, double>
scalar_solve(
        const boost::property_tree::ptree &prm,
        const MatrixA &A, const MatrixM &M,
        std::vector<double>    const &rhs,
        std::vector<double>          &x
        )
{
    prof.tic("setup");
    amgcl::make_solver<
        amgcl::runtime::preconditioner<Backend>,
        amgcl::runtime::iterative_solver<Backend>
        > solve(M, prm);
    prof.toc("setup");

    std::cout << solve << std::endl;

    scoped_tic t(prof, "solve");
    return solve(A, rhs, x);
}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    namespace po = boost::program_options;
    namespace io = amgcl::io;

    using amgcl::prof;
    using std::vector;
    using std::string;

    po::options_description desc("Options");

    desc.add_options()
        ("help,h", "Show this help.")
        ("prm-file,P",
         po::value<string>(),
         "Parameter file in json format. "
        )
        (
         "prm,p",
         po::value< vector<string> >()->multitoken(),
         "Parameters specified as name=value pairs. "
         "May be provided multiple times. Examples:\n"
         "  -p solver.tol=1e-3\n"
         "  -p precond.coarse_enough=300"
        )
        ("high,A",
         po::value<string>()->required(),
         "High-order system matrix in the MatrixMarket format. "
        )
        ("low,M",
         po::value<string>()->required(),
         "Low-order system matrix in the MatrixMarket format. "
        )
        (
         "rhs,f",
         po::value<string>(),
         "The RHS vector in the MatrixMarket format. "
         "When omitted, a vector of ones is used by default. "
        )
        (
         "binary,B",
         po::bool_switch()->default_value(false),
         "When specified, treat input files as binary instead of as MatrixMarket. "
         "It is assumed the files were converted to binary format with mm2bin utility. "
        )
        (
         "single-level,1",
         po::bool_switch()->default_value(false),
         "When specified, the AMG hierarchy is not constructed. "
         "Instead, the problem is solved using a single-level smoother as preconditioner. "
        )
        (
         "initial,x",
         po::value<double>()->default_value(0),
         "Value to use as initial approximation. "
        )
        (
         "output,o",
         po::value<string>(),
         "Output file. Will be saved in the MatrixMarket format. "
         "When omitted, the solution is not saved. "
        )
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    boost::property_tree::ptree prm;
    if (vm.count("prm-file")) {
        read_json(vm["prm-file"].as<string>(), prm);
    }

    if (vm.count("prm")) {
        BOOST_FOREACH(string pair, vm["prm"].as<vector<string> >())
        {
            using namespace boost::algorithm;
            vector<string> key_val;
            split(key_val, pair, is_any_of("="));
            if (key_val.size() != 2) throw po::invalid_option_value(
                    "Parameters specified with -p option "
                    "should have name=value format");

            prm.put(key_val[0], key_val[1]);
        }
    }

    size_t rows;
    vector<ptrdiff_t> Aptr, Acol, Mptr, Mcol;
    vector<double> Aval, Mval, rhs, x;

    {
        scoped_tic t(prof, "reading");

        string Afile  = vm["high"].as<string>();
        string Mfile  = vm["low"].as<string>();
        bool   binary = vm["binary"].as<bool>();

        if (binary) {
            io::read_crs(Afile, rows, Aptr, Acol, Aval);

            size_t Mrows;
            io::read_crs(Mfile, Mrows, Mptr, Mcol, Mval);

            precondition(Mrows == rows, "Number of dofs in M differs from that of A");
        } else {
            size_t cols, Mrows, Mcols;

            boost::tie(rows, cols) = io::mm_reader(Afile)(Aptr, Acol, Aval);
            precondition(rows == cols, "Non-square matrix A");

            boost::tie(Mrows, Mcols) = io::mm_reader(Mfile)(Mptr, Mcol, Mval);

            precondition(Mrows == Mcols, "Non-square matrix M");
            precondition(Mrows == rows, "Number of dofs in M differs from that of A");
        }

        if (vm.count("rhs")) {
            string bfile = vm["rhs"].as<string>();

            size_t n, m;

            if (binary) {
                io::read_dense(bfile, n, m, rhs);
            } else {
                boost::tie(n, m) = io::mm_reader(bfile)(rhs);
            }

            precondition(n == rows && m == 1, "The RHS vector has wrong size");
        } else {
            rhs.resize(rows, 1.0);
        }
    }

    x.resize(rows, vm["initial"].as<double>());

    size_t iters;
    double error;

    if (vm["single-level"].as<bool>())
        prm.put("precond.class", "relaxation");

    boost::tie(iters, error) = scalar_solve(
            prm,
            boost::tie(rows, Aptr, Acol, Aval),
            boost::tie(rows, Mptr, Mcol, Mval),
            rhs, x
            );

    if (vm.count("output")) {
        scoped_tic t(prof, "write");
        amgcl::io::mm_write(vm["output"].as<string>(), &x[0], x.size());
    }

    std::cout << "Iterations: " << iters << std::endl
              << "Error:      " << error << std::endl
              << prof << std::endl;
}
